from fastapi import FastAPI
from routes.employeesRoute import employee

app = FastAPI(
    title = "API Employees Cidenet",
    description = "API for consult, create and delete employees",
    version = "0.1",
    openapi_tags = [{
        "name": "employees",
        "description": "employees routes"
    }]
)
app.include_router(employee)