from datetime import datetime
from service.employeeService import getAllEmployeeService, createEmployeeService, deleteEmployeeService
from controller.utils import createEmail, transformResponse, validateNames
from fastapi import Response
from starlette.status import HTTP_406_NOT_ACCEPTABLE
import uuid


def getAllEmployeesController():
    res = getAllEmployeeService()
    response = transformResponse(res["message"])
    return { "status": res["status"], "message": response }

def createEmployeeController(employee):
    names = validateNames(employee.firtsName,employee.lastName,employee.othersNames)
    if names[0]:
        newEmployee = {"firstName": names[1],
                    "lastName": names[2], 
                    "othersNames": names[3],
                    "country": employee.country}
        newEmployee["id"] = str(uuid.uuid4())
        newEmployee["createdAt"] = str(datetime.now())
        newEmployee["email"] = createEmail(names[1],names[2],employee.country)

        return createEmployeeService(newEmployee)
    else:
        return { "status": Response(status_code=HTTP_406_NOT_ACCEPTABLE),  "message": "Controller Error: Text string too long or unsupported characters" }

def deleteEmployeeController(id: str):
    return deleteEmployeeService(id)
