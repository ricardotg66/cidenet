import re
from service.employeeService import getEmployeesService


def createEmail(firstName: str, lastName: str, country: str):
    IdEmail = searchIdEmail(firstName, lastName)

    if country == "CO":
        fN = firstName.lower()
        lN = lastName.lower().replace(" ","")
        domain = "@cidenet.com.co"
        email = fN +'.'+ lN + '.' + IdEmail + domain
        return email
    
    elif country == "US":
        fN = firstName.lower()
        lN = lastName.lower().replace(" ","")
        domain = "@cidenet.com.us"
        email = fN +'.'+ lN + '.' + IdEmail + domain
        return email
    else:
        return "Error. Email not created"
    

def searchIdEmail(firstName: str, lastName: str):
    patron = r"\d+"
    employees = getEmployeesService(firstName, lastName)
    
    ids = []
    if len(employees) > 0:
        for emp in employees:
            i = re.findall(patron, emp[5])
            if i:
                ids.append(int(i[0]))
        
        idMax = max(ids)
        if idMax > 0:
            return f"{idMax+1}"
        else: return "1"
    else:
        return "1"

def transformResponse(res):
    diccionarios = []
    for tupla in res:
        diccionario = {'id': tupla[0], 'firstName': tupla[1], 'lastName': tupla[2], 'othersNames': tupla[3], 'country': tupla[4], 'email': tupla[5], 'createdAt': tupla[6]}
        diccionarios.append(diccionario)

    response = {}
    for index in range(0,len(diccionarios)):
        response[str(index)] = diccionarios[index]

    return response

def validateNames(firstName:str,lastName:str,othersNames:str):
    if (len(firstName) or len(lastName) or len(othersNames)) > 20: return [False]

    firstName = firstName.upper()
    lastName = lastName.replace(" ", "").upper()
    othersNames = othersNames.upper()
    names = [firstName, lastName, othersNames]
    for n in names:
        if not all(c.isalpha() and c.isupper() and c not in ['Ñ', 'Á', 'É', 'Í', 'Ó', 'Ú'] for c in n): return [False]
    
    return [True, firstName, lastName, othersNames]



