from sqlalchemy import Table, Column
from sqlalchemy.sql.sqltypes import String
from config.db import engine, meta

employees = Table("Employees", meta,
                  Column("id", String(255), primary_key=True),
                  Column("firstName", String(20)),
                  Column("lastName", String(20)),
                  Column("othersNames", String(50)),
                  Column("country", String(20)),
                  Column("email", String(300)),
                  Column("createdAt", String(50)))

meta.create_all(engine)