# API Employee Cidenet
## Ejercicio técnico - Instructivo de uso

La API fue desarrollada en Python 3.11.2 utilizando el framework FastAPI en un entorno virtual creado en anaconda. Por consiguiente, se recomienda crear un entorno virtual en anaconda o un software similar e instalar los paquetes que se incluyen en el archivo **"requirements.txt"**.

La base de datos utilizada es **MySQL v8.0.32**. Es requisito tener instalado **MySQL** para utilizar la API tal cual fue desarrollada. Para otras bases de datos, se deberá consultar la documentación para la respectiva conexión.

**Conexión a la base de datos:** Se debe crear un archivo __.env__ con la siguiente estructura:
```
USER_DB = *my_user*
PASS_DB = *my_password*
DOMAIN_DB = *my_domain*  
PORT_DB = 3306 
TABLE_DB = employee
```

El puerto 3306 es el puerto utilizado por defecto por MySQL y la tabla employee es a donde se apunta a guardar los datos descritos por el modelo de datos (ver carpeta models). El archivo .env se debe almacenar en la raíz del proyecto.

Luego de instalar los paquetes y la base de datos, la api se puede ejecutar por consola con el siguiente comando: 
```sh
uvicorn app:app
```
y se debe acceder a la dirección http://localhost:8000/

La api provee una página de pruebas a la cual se puede acceder por medio de http://localhost:8000/docs

Link del vídeo funcionamiento: https://drive.google.com/drive/folders/1vvaAB58fyHzY34dd9efFShnNMKnLmnut?usp=sharing