from fastapi import APIRouter
from schema.employee import Employee
from controller.employeeController import getAllEmployeesController, createEmployeeController, deleteEmployeeController

employee = APIRouter()

@employee.get("/", tags=["employees"])
def InfoApi():
    response = {
        "name": "Api v0.1 test CRUD employees",
        "author": "Ricardo José Trullo Guerrero",
        "company": "Cidenet",
        "language": "Python",
        "framework": "FastApi",
        "documentation": "http://localhost:8000/docs"
    }
    return response

@employee.get("/employees", tags=["employees"])
def getEmployeesRoute():
    return getAllEmployeesController()

@employee.post("/employees", tags=["employees"])
def createEmployeeRoute(employee: Employee):
    return createEmployeeController(employee)

@employee.delete("/employees/{id}", tags=["employees"])
def deleteEmployeeRoute(id: str):
    return deleteEmployeeController(id)