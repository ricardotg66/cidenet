from pydantic import BaseModel
from typing import Optional

class Employee(BaseModel):
    id: Optional[str]
    firtsName: str
    lastName: str
    othersNames: Optional[str]
    country: str
    email: Optional[str]
    createdAt: Optional[str]

