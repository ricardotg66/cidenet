from config.db import conx
from models.employee import employees
from fastapi import Response
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_200_OK, HTTP_204_NO_CONTENT

def getAllEmployeeService():
    try:
        message = conx.execute(employees.select()).fetchall()
        return { "status": Response(status_code=HTTP_204_NO_CONTENT),  "message": message}
    
    except Exception:
        return { "status": Response(status_code=HTTP_500_INTERNAL_SERVER_ERROR),  "message": "Server Error: Consult Employee Service"}


def createEmployeeService(newEmployee):
    try:
        conx.execute(employees.insert().values(newEmployee))
        conx.commit()

    except Exception:
        return { "status": Response(status_code=HTTP_500_INTERNAL_SERVER_ERROR),  "message": "Server Error: Create Employee Service"}
        
    else:
        message = conx.execute(employees.select().where(employees.c.id == newEmployee["id"])).fetchone()
        return { "status": Response(status_code=HTTP_200_OK),  "message": str(message) }


def getEmployeesService(firstName: str,lastName: str):
    try:
        response = conx.execute(employees.select().filter(employees.c.firstName == firstName, employees.c.lastName == lastName )).fetchall()
        return response
    
    except Exception:
        return "Request Empty"


def deleteEmployeeService(id: str):
    try:
        conx.execute(employees.delete().where(employees.c.id == id))
        conx.commit()
        return { "status": Response(status_code=HTTP_204_NO_CONTENT),  "message": "Delete Service: Employee deleted success" }
    
    except Exception:
        return { "status": Response(status_code=HTTP_500_INTERNAL_SERVER_ERROR),  "message": "Server Error: Delete Employee Service"}