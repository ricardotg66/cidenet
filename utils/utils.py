def createEmail(firstName: str, lastName: str, country: str):
    ID = 0

    if country == "CO":
        email = f'{firstName.lower()} + "." + {lastName.lower()} + {ID} + "@cidenet.com.co"'
        return email
    elif country == "EU":
        email = f'{firstName.lower()} + "." + {lastName.lower()} + {ID} + "@cidenet.com.us"'
        return email
    else:
        return "Error. Email not created"
    
def transformResponse(res):
    response = {
        "id": f"{res[0][0]}",
        "firstName": f"{res[0][1]}",
        "lastName": f"{res[0][2]}", 
        "othersNames": f"{res[0][3]}",
        "country": f"{res[0][4]}",
        "email": f"{res[0][5]}",
        "createdAt": f"{res[0][6]}"
        }
    return response